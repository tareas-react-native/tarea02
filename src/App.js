/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  StatusBar,
  View,
  Text,
} from 'react-native';
import DishList from './components/DishList/DishList';
import Dropdown from './components/Dropdown';

const styles = StyleSheet.create({
  dropdownItem: {
    height: 50,
    backgroundColor: "#95a5a6",
    borderColor: "#000000",
    borderWidth: 1,
    alignItems: "flex-start",
    justifyContent: "center",
    paddingLeft: 20
  }
})


const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{flex:1}}>
          <DishList></DishList>
      </SafeAreaView>
    </>
  );
};

export default App;
