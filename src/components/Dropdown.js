import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const styles = StyleSheet.create({
    container: {
        margin: 20,
    },
    dropdown: {
         flexDirection: "row",
         alignItems: "center",
         justifyContent: "space-between",
         backgroundColor: "#2980b9",
         padding: 20
    },
    title: {
        color: "#ffffff",
        fontSize: 20
    }
})

export default class Dropdown extends Component {

    constructor(props){
        super(props);

        this.state = {
            open: false
        }
    }

    toggle = () => this.setState(({open}) => ({open: !open}) )

    render() {

        const { children } = this.props
        const { open } = this.state

        return (
            <View style={styles.container}>
                <TouchableOpacity
                    onPress={()=> this.toggle()}>
                    <View style={styles.dropdown}>
                        <Text style={styles.title}>DropDown</Text>
                        <Text>+</Text>
                    </View>
                </TouchableOpacity>
                {open && children}
            </View>
        )
    }
}
