import React, { Component } from 'react'
import { Alert, FlatList, Linking, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import HorizontalScroll from '../HorizontalScroll'
import {dishTitles, dishData, baseUri} from '../../rowData'
import DishCard from './DishCard'
import BotonTotal from './BotonTotal'

const styles = StyleSheet.create({
    flatListContainer:{
        padding: 10,
        width: '100%',
        marginBottom: 10

    },
    buttonTotalDishesSelected:{
        backgroundColor: "#8e44ad",
        borderBottomLeftRadius: 7,
        borderBottomRightRadius: 7,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    tituloPlatos:{
        fontWeight: "bold",
        fontSize: 30,
        paddingLeft: 20
    }
})

export default class DishList extends Component {

    constructor(props){
        super(props)

        this.state = {
            dishes: dishData,
            selectedDishes: [],
            cartDishes: []
        }
    }

    selectDishes = (name) => {
        //console.log(name)

        const { dishes } = this.state

        const dishesFilter = dishes.filter((d) => d.cuisine === name)

        //console.log(dishesFilter)

        this.setState({selectedDishes: dishesFilter})
    }

    addDishes = (title) => {
        /*
        console.log("plato seleccionado ", title)
        */
        
        const { dishes, cartDishes } = this.state
        const dishSelected = dishes.find((d) => d.title === title)

        const newList = [...cartDishes, dishSelected];

        console.log("newwList ", newList)
        this.setState({cartDishes: newList})

        Alert.alert('Muy Bien!',`Plato agregado al carrito`); 

    }

    emptyCartDishes = () => {
        const { cartDishes } = this.state

        if(cartDishes.length)
        {
            Alert.alert('Confirmacion','¿Quieres eliminar todo el carrito?', [
                {
                    text: "Borra todo!!",
                    onPress: () => this.setState({cartDishes: []})
                },
                {
                    text: "No, me arrepentí"
                }
            ]); 
        }
        
    }

    render() {

        const {selectedDishes, cartDishes} = this.state
        return (
            <>
                <HorizontalScroll style={{margin: 10}} onPress={this.selectDishes}></HorizontalScroll>
                <FlatList
                    data={selectedDishes} 
                    style={styles.flatListContainer}
                    keyExtractor={({id}) => id.toString()}
                    renderItem={({item: {title, readyInMinutes, servings, image}}) => (
                        <DishCard
                            title={title}
                            readyInMinutes={readyInMinutes}
                            servings={servings}
                            image={image}
                            addDishes={this.addDishes}
                            mostrarBotonAgregar={true}
                        >
                        </DishCard>
                    )}
                    ListEmptyComponent={() => (
                        <View>
                            <Text>
                                No hay platos disponibles
                            </Text>
                        </View>
                    )}
                >
                </FlatList>

                <Text style={styles.tituloPlatos}>Platos</Text>
                <FlatList
                    data={cartDishes} 
                    style={styles.flatListContainer}
                    keyExtractor={({id}) => id.toString()}
                    renderItem={({item: {title, readyInMinutes, servings, image}}) => (
                        <DishCard
                            title={title}
                            readyInMinutes={readyInMinutes}
                            servings={servings}
                            image={image}
                            addDishes={this.addDishes}
                            mostrarBotonAgregar={false} 
                        >
                        </DishCard>
                    )}
                    ListEmptyComponent={() => (
                        <View>
                            <Text>
                                No hay platos agregados al carrito
                            </Text>
                        </View>
                    )}
                >
                </FlatList>
                <BotonTotal total={cartDishes.length} emptyCartDishes={this.emptyCartDishes}></BotonTotal>
            </>
        )
    }
}
