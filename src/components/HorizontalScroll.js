import React, { Component } from 'react'
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { dishTitles } from '../rowData'
import PropTypes from 'prop-types'

const styles = StyleSheet.create({
    container:{
        margin: 20,
        backgroundColor: "green"
    },
    button: {
        padding: 5,
    },
    buttonText: {
        color: "#ecf0f1",
    },
    

}) 

export default class HorizontalScroll extends Component {


    render() {
        const {item,  style, onPress} = this.props
        return (
            <View style={StyleSheet.flatten([styles.container, style])}>
                <FlatList 
                    data={item || dishTitles} 
                    keyExtractor={({id}) => id.toString()}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    renderItem={({item: {id, name}}) => (
                        <TouchableOpacity
                            onPress={() => onPress(name)}
                            style={styles.button}>
                            <Text style={styles.buttonText}>{name}</Text>
                        </TouchableOpacity>
                    )}
                />
            </View>
        )
    }
}
 
/* requeridos */
HorizontalScroll.propType = {
    style: PropTypes.shape({}),
    items: PropTypes.array,
    onPress: PropTypes.func,
};

/* que se devuelve por defecto */
HorizontalScroll.defaultProps = {
    style: {},
    items: [],
    onPress: () => {},
};